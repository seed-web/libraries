<?php
 /**
  *
  * @package Interface
  *
  */

/**
*
* @subpackage Classes
*
*/
#===================================================================

class Field extends TreeNode {

#===================================================================


    //-------------------------------------------------------------
    function test($data) {

        $field=$this->data->get("name");
        $optional=$this->data->get("optional",null);

        if (  isset( $field,$data) and $data[$field] != "" ) {
            return true;
        }elseif ( $optional==null ) {
            return false;
        }
        return true;
    }

    //-------------------------------------------------------------
    function get_dataX($data) {
        $field=$this->data->get("name");
        $optional=$this->data->get("optional",null);
        $default=$this->data->get("default",null);

        if (  array_key_exists($field,$data) ) {
            return $data[$field];

        }elseif ( $default!=null ) {
            return $default;


        }
        return null;
    }

    //-------------------------------------------------------------

}
#===================================================================


?>
