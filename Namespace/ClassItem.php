<?php
 /**
  *
  * @package Interface
  *
  */

/**
*
* @subpackage Classes
*
*/
#===================================================================

class ClassItem extends ScriptNode {

#===================================================================


    //-------------------------------------------------------------
    function test($data) {

        //var_dump($data);
        foreach($this->children->by_class("Field") as $field){
            $result=$field->test($data);
            //echo $field->get("name");
            //echo $result ? 'true' : 'false';
            if($result==false){return false;}
            

        }
        return true;
    }


    //-------------------------------------------------------------
    function get_dataX($data) {

        $result=[];
        foreach($this->children->by_class("Field") as $field){
            $result[$field->data->get("name")]=$field->get_data($data);
        }
        return $result;
    }

    //-------------------------------------------------------------
}
#===================================================================


?>
