<?php
 /**
  *
  * @package Interface
  *
  */

/**
*
* @subpackage Classes
*
*/
#===================================================================

class TreeClass extends ClassItem {

#===================================================================

    //-------------------------------------------------------------
    function is_instance($node) {

        if(get_class($node)== "TreeNode"){
            return $cls == $node->get("__class__");
        }
        $clsname=$this->get("name");
        return $node instanceof $clsname;
    }

    //-------------------------------------------------------------
    function methods($node,$filter="") {


        foreach($this->search("*/[ClassMethod]")->iter() as $action){
            //echo $action->get("name");
            if ($filter && str_starts_with($action->get("name"), $filter)) {
                yield $action;
            }else if(! $filter){
                yield $action;
            }
        }

    }
    //-------------------------------------------------------------
    function attrs($node,$filter="") {


        foreach($this->search("*/[Field]")->iter() as $field){
            //echo $action->get("name");
            if ($filter && str_starts_with($field->get("name"), $filter)) {
                yield $field;
            }else if(! $filter){
                yield $field;
            }
        }

    }

    //-------------------------------------------------------------
    function onPreSetup() {


    
    }


    //-------------------------------------------------------------
    function query($node,$request) {

        if(array_key_exists("action",$request)){
            $action=$this->find($request["action"]);
            if($action){
                $action->query($node,$request);    
            }   
        }

    }



    //-------------------------------------------------------------
    function get_url() {
        return "/ui/main.php?path=/tree/new.php&cls=".$this->data->get("name");
    }
    //-------------------------------------------------------------
    function new_form($data=[]) {

?>
        <form  id="add_link" onsubmit="return false;" data-url="<?=$this->get_url()?>">
        <h1><?=$this->data->get("name")?></h1>
          <input type="hidden" id="cls" name="cls"  value="<?=$this->data->get("name")?>">
<?php
        foreach($this->children->by_class("Field") as $field){
?>
          <label for="fname"><?=$field->data->get("name")?></label>
          <input type="text" id="<?=$field->data->get("name")?>" name="<?=$field->data->get("name")?>"  value="<?=$field->get_data($data)?>">

<?php
        }
?>

          <button type="submit" onclickX="add_link.show();">new</button>
        </form> 

<?php
    }
    //-------------------------------------------------------------


}
#===================================================================


?>
