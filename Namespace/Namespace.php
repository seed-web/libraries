<?php
 /**
  *
  * @package Interface
  *
  */

/**
*
* @subpackage Classes
*
*/
#===================================================================

class TreeNamespace extends ScriptNode {

#===================================================================


    //-------------------------------------------------------------
    function onPreSetup() {

    }

    //-------------------------------------------------------------
    function classes($node) {


        foreach( $this->search("/**/[TreeClass]")->iter() as $cls){

            if($cls->is_instance($node) ){
                yield $cls;
            }

        }
        
    }

    //-------------------------------------------------------------
    function ask($node,$method,$request) {
        $elt=null;
        $mth=null;

        foreach( $this->search("/**/[TreeClass]")->iter() as $cls){
            $cls_name=$cls->get("name");

            if($node instanceof $cls_name) {
                $cls_mth=$cls->find($method);
                if($cls_mth){
                    $mth=$cls_mth;
                }

            }  
        } 

        if($mth){

            $mth->selected=$node;
            $mth->call($request);
        }else{

            echo "no element ".$method." for node ".$node->path();
        }

    }

    //-------------------------------------------------------------
    function methods($node,$filter="") {
        foreach($this->classes($node) as $cls){

            //echo "<p>".$cls->get("name")."<p>";
            foreach($cls->search("*/[ClassMethod]")->iter() as $action){
                //echo $action->get("name");
                if ($filter && str_starts_with($action->get("name"), $filter)) {
                    yield $action;
                }else if(! $filter){
                    yield $action;
                }
            }


        }
    }

    //-------------------------------------------------------------
}
#===================================================================


?>
