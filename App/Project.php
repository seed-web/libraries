<?php
 /**
  *
  * @package App
  *
  */
//=================================================================
class Project extends ServerNode {
//=================================================================
    //-------------------------------------------------------------
    function onInit() {
        parent::onInit();
        new InitFiles($this,["event"=>"setup","cls"=>"Service","filter"=>"/*.html"]);
        new InitFiles($this,["event"=>"setup","cls"=>"Service","filter"=>"/*.php"]);
    }
    //-------------------------------------------------------------
    function onPreSetupXX() {
        //echo $this->path()."\n";
        parent::onPreSetup();
        //$this->open_init();
        //$this->get('name')



        if ($this->has_file("index.php")) {

            new Service($this,["name"=>'index',"url"=>url($this)."/index.php","icon"=>icon($this),"text"=>$this->get('text')]);
        }

        if ($this->has_file("index.html")) {

            new Service($this,["name"=>'index',"url"=>url($this)."/index.html","icon"=>icon($this),"text"=>$this->get('text')]);
        }

        foreach($this->children->by_class("Service")->iter() as $child){
            $child->set("icon",icon($this));
        }
    }

    //-------------------------------------------------------------
    function onPreSetupX() {
        parent::onPreSetup();
        //$this->directory=new LocalDirectory($this->get("path"));
        //$this->web_pages=new LocalDirectory($this->get("path")."/pages");
        //$this->files_360=new LocalDirectory($this->get("path")."/360");
        
    }
    //-------------------------------------------------------------
    function url_monitor() {
        global  $MONITOR_URL;
        return $MONITOR_URL."?name=".$this->get("name");
    }
    //-------------------------------------------------------------
    function url_editor() {
        global  $EDITOR_URL;
        return $EDITOR_URL."?name=".$this->get("name");
    }
    //-------------------------------------------------------------
    function url_viewer() {
        global  $VIEWER_URL;
        return $VIEWER_URL."&name=".$this->get("name");
    }

    //-------------------------------------------------------------
    function list_360views() {
        return $this->files_360->list_files();
    }
    //-------------------------------------------------------------
    function list_pages() {
        return $this->web_pages->list_files();
    }
    //-------------------------------------------------------------
}


//=================================================================


?>
