<?php
 /**
  *
  * @package App
  *
  */

//=================================================================
class App extends AppComponent {
//=================================================================

    public $globals= null;

    function get_filename(){
        global  $MAIN_PATH;
        return str_replace($MAIN_PATH,"", $this->get("filename"));

    }
    //-------------------------------------------------------------
    function init_request2($request) {

    global $GLOBALS;
        //var_dump($GLOBALS);
        $this->set("path",dirname($GLOBALS["_SERVER"]["SCRIPT_FILENAME"]));
        //echo $this->get("path");
    }
    //-------------------------------------------------------------
    function init_request($request) {

    global $GLOBALS;
        //var_dump($GLOBALS);
        $this->set("path",dirname($this->get("filename")));
        //echo $this->get("path");
    }
    //-------------------------------------------------------------
    function header($node) {
    global $GLOBALS;
        global $PART_DIRECTORY;
        global $STATIC_DIRECTORY;
        include($PART_DIRECTORY."/head.php");

    }
    //-------------------------------------------------------------
    function footer($node) {
    global $GLOBALS;
        global $PART_DIRECTORY;
        global $STATIC_DIRECTORY;
        include($PART_DIRECTORY."/footer.php");

    }
    //-------------------------------------------------------------
    function exec_request($request) {

        global $PART_DIRECTORY;
        global $STATIC_DIRECTORY;

        if( $page=="included" ){

                if( array_key_exists("selected",$request) && $request["selected"]!=""){
                    $selected=$this->find($request["selected"]);

                    if(! $selected){
                        echo "no selection ".$request["selected"];
                        exit();
                    }
                $node->selected=$selected;
                }else{
                    $node->selected=$this;
                }
                include($PART_DIRECTORY."/head.php");
                //myplot_tree("call",$node->call($request));
                $node->call($request);
                include($PART_DIRECTORY."/footer.php");
        }else{
                if( array_key_exists("selected",$request) ){
                    $node->selected=$this->find($request["selected"]);
                }else{
                    //myplot_tree("call",$node->selected=$this);
                    $node->call($request);
                }
                $node->call($request);
        }


    }
    //-------------------------------------------------------------
    function onInit() {
        parent::onInit();
        global $_SERVER;
        //var_dump($_SERVER);
        new GlobalVariable($this,["name"=>"DOCUMENT_ROOT","value"=>$_SERVER["DOCUMENT_ROOT"]]);
        //new GlobalVariable($this,["name"=>"DOCUMENT_URL","value"=>$_SERVER["HTTP_REFERER"]]);
        parent::onInit($this);
    }



    //-------------------------------------------------------------
    function get_views2($name,$project,$cls,$request) {
        global $API_URL;
        $content=new $cls();

        foreach($this->find("/classes")->classes($project) as $class_node){

        foreach($class_node->all()->by_class("Method") as $method){

            if($method->get("name")==$name){

                foreach($method->children->by_class("MethodInclude") as $action){
                    $url=$API_URL."?action=call&path=".$action->path->get()."&selected=".$project->path->get();
                    //echo $url;
                    //$method->query($project,$request);
                    new XmlView($content,["name"=>$action->get("name"),
                                        "url"=>$url]);
                }
            }
        }
        }
        return $content;
    }
    //-------------------------------------------------------------
    function get_views($name,$project,$cls) {

        $content=new $cls();
        foreach($this->plugins() as $plugin){
            $view=$plugin->get_view($name,$project);
            if($view!=null){
                new XmlView($content,["name"=>$plugin->get("name"),
                                    "url"=>$view]);
            }
        }
        return $content;
    }
    //-------------------------------------------------------------


}
//=================================================================


?>
