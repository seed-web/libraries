<?php
 /**
  *
  * @package App
  *
  */

/**
  *
  * @subpackage Plugin
  *
  */
//=================================================================
class PluginPage extends ServerNode {
//=================================================================


    //-------------------------------------------------------------
    function call($request=[]){
        //echo $this->get("path");
        $filename=$this->get("path");
        $view=$this;
        $plugin=$this->parent;
        $application=$this->root();
        include($filename);
    }
    //-------------------------------------------------------------
}
//=================================================================

?>
