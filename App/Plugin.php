<?php
 /**
  *
  * @package App
  *
  */

//=================================================================
class Plugin extends Project {
//=================================================================
    //-------------------------------------------------------------
    function urlX() {
        global $API_URL;
        global $MAIN_PATH;

        if ($this->has_file("index.php")) {
            return str_replace($MAIN_PATH,"", $this->get("path"))."/index.php?";

        } else if ($this->has_file("index.xml")) {
            $p= str_replace($MAIN_PATH,"", $this->get("path"))."/index.xml";
            return $API_URL."?filename=".$p."&start=/main&";
        }else{
            echo"probleme";
        }
    }
    //-------------------------------------------------------------
    function onInit() {
        parent::onInit();

        new InitFiles($this,["event"=>"setup","cls"=>"PluginPage","filter"=>"/pages/*.php"]);
        new InitFiles($this,["event"=>"setup","cls"=>"PluginFileView","filter"=>"/views/*.php"]);
        new InitFiles($this,["event"=>"setup","cls"=>"PluginAction","filter"=>"/api/*.php"]);
    }
    //-------------------------------------------------------------
    function onPreSetup() {
        global $API_URL;
        global $MAIN_PATH;
        parent::onPreSetup();

        foreach($this->children->by_class("Service")->iter() as $child){
            //$child->set("icon",$this->front_image());
        }
        foreach($this->children->by_class("PluginPage")->iter() as $child){
            //$child->set("icon",$this->front_image());
            $this->set("has_pages","true");
        }
        if ($this->has_file("index.php")) {
            $url= str_replace($MAIN_PATH,"", $this->get("path"))."/index.php?";
            //$this->set("url",$url);
            $name="index";
            //$name=$this->get('name');
            //$icon=icon();
            new Service($this,["name"=>$name,"url"=>$url,"text"=>$this->get('text'),"sys"=>"true"]);//,"icon"=>$icon

        } else if ($this->has_file("index.xml")) {
            $p= str_replace($MAIN_PATH,"", $this->get("path"))."/index.xml";
            $url=$API_URL."?filename=".$p."&path=/main&";
            //$this->set("url",$url);
            $name="index";
            //$name=$this->get('name');

            //$icon=icon();
            new Service($this,["name"=>$name,"url"=>$url,"text"=>$this->get('text'),"sys"=>"true"]);//,"icon"=>$icon
        }



        foreach($this->search("/**/[TreeClass]")->iter() as $cls ){
            $path=$this->get("path")."/".$cls->get("name");


            $dirs= new DiscNode(null,["path"=>$path]);
            new InitFiles($dirs,["event"=>"init"]);

            if($dirs->is_real()){
                $dirs->setup();
                //echo $path;
                //tree($dirs);
                //var_dump(glob($path."/*" ));
                foreach($dirs->search("*/[DiscNode]")->iter() as $elt){
                        $name= $elt->get("filename");
                        $ext=$elt->get("ext");
                        $method=$cls->make_node($name,"ClassMethod",[]);
//echo $method->path();
                        new CallInclude($method,["name"=>$this->get("name"),"path"=>$elt->get("path")]);
                }
            }
        }

//$this->plot();
    }
    //-------------------------------------------------------------
    function get_view($view_name,$project) {
        global $API_URL;
        $node=$this->find($view_name);

        if($node!=null and $project != null){
            return $API_URL."?action=view&path=".$node->get_path()."&name=".$project->get("name");
        }elseif($node!=null){
            return $API_URL."?action=view&path=".$node->get_path();
        }else{
            return null;
        }
    }
    //-------------------------------------------------------------
}
//=================================================================

?>
