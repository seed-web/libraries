<?php
 /**
  *
  * @package Data
  *
  */


 /**
  *
  * @subpackage File
  *
  */
//=================================================================

class DiscNode extends Data {

//=================================================================
    public $content_extentions=array("md","odt","json","xml","txt");
    public $image_extentions=array("jpg","jpeg","JPG","JPEG","png","PNG","gif","GIF","svg");
    public $embeded_extentions=array("pdf","html","php");
    public $video_extentions=array("mp4","webm","swf","flv");
    public $audio_extentions=array("wav","WAV","3ga","mp3","opus","m4a","ogg");
    //-------------------------------------------------------------
    function onInit(){
        parent::onInit();
        if($this->is_file()){
            $this->set("type","file");

        }else if($this->is_dir()){
            $this->set("type","dir");

        }
    }
    //-------------------------------------------------------------
    function onPreSetup(){
        parent::onPreSetup();
        $directory=$this->get("path");
        $this->set("name",basename($directory));
        $this->set("basename",basename($directory));
        $this->set("realpath",realpath($directory));
        //echo realpath($directory)."\n";
        //show($this);

        if($this->is_file()){
            $this->set("type","file");
            $this->set("filename", pathinfo($directory, PATHINFO_FILENAME));
            $this->set("ext",pathinfo($directory, PATHINFO_EXTENSION));

        }else if($this->is_dir()){
            $this->set("type","dir");
            //$this->set("filename",basename($directory));
            //$this->set("ext","");
        }

        if( $this->is_content()){
            $this->set("media","content");
        }elseif($this->is_image()){
            $this->set("media","image");
        }elseif($this->is_embeded()){
            $this->set("media","embeded");
        }else if($this->is_video()){
            $this->set("media","video");
        }else if($this->is_audio()){
            $this->set("media","audio");
        }

    }

    //-------------------------------------------------------------
    function filepath() {return $this->get("path");}
    //-------------------------------------------------------------
    function get_filepath($filename) {
        return $this->get("path")."/".$filename;
    }
    //-------------------------------------------------------------
    function is_real() {
        if(  file_exists($this->get("path"))  ){
            return true;
        }
    }
    //-------------------------------------------------------------
    function has_file($filename) {
        if( file_exists($this->get_filepath($filename))  ){
            return true;
        }
    }
    //-------------------------------------------------------------
    function is_file() {
        if( is_file($this->get("path")) ){
            return true;
        }
    }
    //-------------------------------------------------------------
    function is_dir() {
        if( is_dir($this->get("path")) ){
            return true;
        }
    }
    //-------------------------------------------------------------
    function is_content() {

            return in_array($this->get("ext"),$this->content_extentions);
        }
    //-------------------------------------------------------------
    function is_image() {

            return in_array($this->get("ext"),$this->image_extentions);
        } 
    //-------------------------------------------------------------
    function is_embeded() {

            return in_array($this->get("ext"),$this->embeded_extentions);
        } 
    //-------------------------------------------------------------
    function is_video() {

            return in_array($this->get("ext"),$this->video_extentions);
        } 
    //-------------------------------------------------------------
    function is_audio() {

            return in_array($this->get("ext"),$this->audio_extentions);
        } 
    //-------------------------------------------------------------
    function get_filenode($filename,$cls="DiscNode") {

        $files=$this->search("*/[".$cls."]/[name=".$filename."]");

        if($files->is_empty()){
            return new $cls($this,["path"=>$this->get_filepath($filename)]);
        }else{
            return $files->first();
        }
    }
    //-------------------------------------------------------------
    function get_xmlfile($filename,$cls="DiscNode",$root=null) {

        $files=$this->search("*/[".$cls."]/[name=".$filename."]");

        if($files->is_empty()){
            //$filename=$this->get_filepath($filename);
            $xml_parser=new DomXmlRead();
            $new_node=$xml_parser->readfile($filename,$root);
            $new_node->set("basename",basename($filename));
            $new_node->set("path",$filename);
            $new_node->attach($this);
            return $new_node;
        }else{
            return $files->firsts();
        }
    }  
    //-------------------------------------------------------------

}
 /**
  *
  * @subpackage File
  *
  */

#=================================================================

class HasFile extends Test {

#=================================================================
    //-------------------------------------------------------------
    function call($node) {
        if($node instanceof DiscNode){
            return $node->has_file($this->get('filename'));
        }
        return false;

    }
    //-------------------------------------------------------------
}
#=================================================================

class InitAll extends Action {

#=================================================================

    //-------------------------------------------------------------
    function onDo($node,$data){

        $directory=$node->filepath();
        $filter=$this->get("filter","/*");
        $cls=$this->get("cls","DiscNode");
        //echo $directory."\n";
        foreach($this->get_list($node,$directory,$filter,$data)  as $elt){
            //echo $elt."\n";
            $this->make_object($node,$elt,$cls,$data);
        }
    }
    //-------------------------------------------------------------
    function get_list($node,$filepath,$filter,$data){
        //var_dump(glob($filepath.$filter ));
        return glob($filepath.$filter );
    }
    //-------------------------------------------------------------
    function make_object($node,$filepath,$cls,$data){

        $elt=$node->get_filenode(basename($filepath),$cls);
        $elt->set("path",$filepath);
    }
    //-------------------------------------------------------------
}
#=================================================================
 /**
  *
  * @subpackage File
  *
  */
#=================================================================

class InitFiles extends InitAll {

#=================================================================


    //-------------------------------------------------------------
    function make_object($node,$filepath,$cls,$data){
        //echo $filepath;
        if (is_file($filepath)){
            $elt=$node->get_filenode(basename($filepath),$cls);
            $elt->set("path",$filepath);
        }
    }
    //-------------------------------------------------------------
}
#=================================================================
 /**
  *
  * @subpackage File
  *
  */
#=================================================================

class InitXmlFiles extends InitAll {

#=================================================================

    //-------------------------------------------------------------
    function make_object($node,$filepath,$cls,$data){
    if(is_file($filepath) && basename($filepath)!="__init__.xml"){
        $node->get_xmlfile($filepath,$cls,$node);
    }
    }
    //-------------------------------------------------------------
}
 /**
  *
  * @subpackage File
  *
  */
#=================================================================

class InitDirs extends InitAll {

#=================================================================

    //-------------------------------------------------------------
    function get_list($node,$filepath,$filter,$data){
        //echo $filepath.$filter."\n";
        return glob($filepath .$filter, GLOB_ONLYDIR);
    }
    //-------------------------------------------------------------
}
 /**
  *
  * @subpackage File
  *
  */
#=================================================================

class InitConfigDirs extends InitDirs {

#=================================================================

    //-------------------------------------------------------------
    function make_object($node,$filepath,$cls,$data){
        //echo $filepath."\n";
        $xml_parser=new DomXmlRead();

        $init_file=$filepath."/".$this->get("init_file","__init__.xml");
        if(is_file($init_file)){

            $new_node=$xml_parser->readfile($init_file);
            $new_node->set("name",basename($filepath));
            $new_node->set("path",$filepath);
            $new_node->attach($node);
        }else{
            $node->get_filenode(basename($filepath),$cls);
        }
    }
    //-------------------------------------------------------------
}
 /**
  *
  * @subpackage File
  *
  */
#=================================================================

class Mkdir extends Action {

#=================================================================

    //-------------------------------------------------------------
    function onDo($node,$data){

        $directory=$node->filepath();
        $new_name=$data->get("new_name");
        $new_path=$directory."/".$new_name;

        if ($node->is_real() and ! $node->has_file($new_name)){
	        mkdir($new_path, 0777);
            return true;
        }else{
            return false;
        }
    }
    //-------------------------------------------------------------
}
 /**
  *
  * @subpackage File
  *
  */
#=================================================================

class Deldir extends Action {

#=================================================================

    //-------------------------------------------------------------
    function onDo($node,$data){

        $directory=$node->filepath();
        if ($node->is_real($directory) ){
            $this->delete($directory);
        }
    }
    //-------------------------------------------------------------
    function delete($filepath){
        //https://paulund.co.uk/php-delete-directory-and-files-in-directory

        if(is_dir($filepath)){

            $files = glob( $filepath . '/*');

            foreach( $files as $file ){
                $this->delete( $file );      
            }

            rmdir( $filepath );

        } elseif(is_file($filepath)) {
            unlink( $filepath );
        }
    }
    //-------------------------------------------------------------
}
 /**
  *
  * @subpackage File
  *
  */
//=================================================================

class Rename extends Action {

#=================================================================

    //-------------------------------------------------------------
    function onDo($node,$data){

        $directory=$node->filepath();
        $new_name=$data->get("new_name");
        $new_path=dirname($directory)."/".$new_name;

        if ($node->is_real() and ! $node->has_file($new_path)){

            rename($directory, $new_path);
            return true;

        }else{
            return false;
        }
    }
    //-------------------------------------------------------------
}
//=================================================================
?>
