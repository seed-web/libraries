<?php
 /**
  *
  * @package Data
  *
  */

 /**
  * @subpackage Formats
  */
class DomJson extends Json {
    //-------------------------------------------------------------
    function onDeserializeNode($data,$root=null) {


            foreach(array_keys($data) as $key){
                //echo $key." - ".$data[$key]."\n";
                $root->make_node($key,$data[$key]["__class__"],$data[$key]) ;
            }

    }

    //-------------------------------------------------------------
    function onSerializeNodeX($nodes) {

            $result=[];
            foreach($nodes as $node){
                foreach($node->get_all() as $elt){
                    $result[$elt->get_path()]=$elt->data;
                }
            }
        return $result;

    }
    //-------------------------------------------------------------

}

?>
