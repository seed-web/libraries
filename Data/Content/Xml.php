<?php
 /**
  *
  * @package Data
  *
  */

 /**
  * @subpackage Formats
  */
//=================================================================

class XmlRead extends Reader {

//=================================================================

    var $first_node;
    //-------------------------------------------------------------
    function onReadFile($url) {
        $this->first_node=null;
        $xmlDoc = new DOMDocument();
        $xmlDoc->load($url);
        return $xmlDoc;
    }
    //-------------------------------------------------------------
    function onRead($data) {
        $this->first_node=null;
        $xmlDoc = new DOMDocument();
        $xmlDoc->loadXML($data);
        return $xmlDoc;
    }
    //-------------------------------------------------------------
    function onDeserializeNode($node,$root=null) {
           switch ( $node->nodeType ) {
                case XML_ELEMENT_NODE:
                    //echo "Found element: $node->tagName";
                    if($node->hasAttributes()) {
                        //echo " with attributes: \n";
                    }
                    $attributes=[];
                    foreach($node->attributes as $attribute ) {
                        $attributes[$attribute->name] = $attribute->value;
                    }
                    //$attributes["text"] = "";
                    //$root=new $node->tagName($root,$attributes);
                    $root=$this->onMakeNode($node->tagName,$root,$attributes);
                    if($this->first_node==null){
                            $this->first_node=$root;
                    }else{

                    }
                    break;
                case XML_CDATA_SECTION_NODE:
                    if ( trim($node->data) ) {
                        //echo "Found character data node: " . htmlspecialchars($node->data) . "\n";
                    }
                    break;
                    
                case XML_TEXT_NODE:
                    if(trim($node->wholeText)) {
                        
                        $root->set("text", $node->wholeText);
                    }
                    break;
            }

            if($node->hasChildNodes()) {
                foreach ($node->childNodes as $child ) {

                if($root  == null){
                    $root=$this->onDeserializeNode($child,$root);
                }else{
                    $this->onDeserializeNode($child,$root);
                }
                }
            }
        return $this->first_node;
    }
    //-------------------------------------------------------------
}
 /**
  * @subpackage Formats
  */
//=================================================================

class XmlWrite extends Writer {

//=================================================================

    //-------------------------------------------------------------
    function onWrite($node) {

        $xmlDoc = new DOMDocument();
        $xmlDoc = new DOMDocument();
        $xmlDoc->formatOutput = true;
        $xmlnode=$this->createXmlNode($node,$xmlDoc,$xmlDoc);
        return $xmlDoc->saveXML($xmlnode);
    }
    //-------------------------------------------------------------
    function onWriteFile($url,$node) {
        $xmlDoc = new DOMDocument();
        $xmlDoc->formatOutput = true;
        $xmlnode=$this->createXmlNode($node,$xmlDoc,$xmlDoc);
        $data=$xmlDoc->saveXML($xmlnode);
        $myfile = fopen($url, "w") or die("Unable to open file!");
        fwrite($myfile, $data);
        fclose($myfile);
    }

    //-------------------------------------------------------------
    function createXmlNode($node,$doc,$root=null) {


        $xmlnode = $doc->createElement(get_class($node));
        $xmlnode = $root->appendChild($xmlnode);

       foreach($node->data as $key=>$value){
            $domAttribute = $doc->createAttribute($key);
            $domAttribute->value = $value;
            $xmlnode->appendChild($domAttribute);
        }


        foreach($node->children->by_class("TreeNode") as $child){
            $this->createXmlNode($child,$doc,$xmlnode);
        }

        return $xmlnode;

    }
    //-------------------------------------------------------------

}
//=================================================================
?>
