<?php
 /**
  *
  * @package Data
  *
  */

 /**
  * @subpackage Formats
  */
class Html extends Xml {

    //-------------------------------------------------------------
    function onRead($data) {
        $xmlDoc = new DOMDocument();
        $xmlDoc->loadHTML($data);
        return $xmlDoc;
    }

    //-------------------------------------------------------------
    function onWrite($node) {

        return $node;
    }

    //-------------------------------------------------------------

}

?>
