<?php
 /**
  *
  * @package Data
  *
  */

 /**
  * @subpackage Formats
  */
//=================================================================
class Json extends TreeParser {
//=================================================================

    //-------------------------------------------------------------
    function onReadFile($url) {
        return json_decode(file_get_contents($url),  true);
    }
    //-------------------------------------------------------------
    function onRead($data) {

        return json_decode($data,  true);
    }
    //-------------------------------------------------------------
    function onDeserializeNode($data,$root=null) {
        //print_r( $data);
        if ( ! empty($data)){
            return $this->onDeserializeTree($data,$root);
        }else{
            return $this->onMakeNode("TreeNode",$root,[]);
        }
    }
    //-------------------------------------------------------------
    function onDeserializeTree($data,$root=null) {

        $node =  $this->onMakeNode("TreeNode",$root,$data->data);
        foreach ($data->children as $child_data ) {
            $child =  $this->onDeserializeTree($node,$child_data);
        }
        return $node;
    }
    //-------------------------------------------------------------
    function onWriteFile($url,$data) {
        $filecontent= json_encode($data);
        $file = fopen($url, "w") or die("Cannot open file.");
        fwrite($file, $filecontent);
        fclose($file);
        return ;
    }
    //-------------------------------------------------------------
    function onWrite($data) {
        //var_dump($data);
        return json_encode($data);
    }

    //-------------------------------------------------------------
    function onSerializeNode($nodes) {

        $result=[];
        foreach($nodes as $node){
echo gettype($node)."\n";
            $result2=[];

            $result2["children"]=[];
            //$result2["data"] =  $node->data->data;
            $result2["children"] =  $this->onSerializeNode($node->children->iter());

            $result[]=$result2;
        }
        return $result;

    }
    //-------------------------------------------------------------
    function read_post($attr) {
        if (isset($_POST[$attr])) {
            $this->data =  json_decode($_POST[$attr], false);
        } else {
            $this->data=NULL;
        }
    }
    //-------------------------------------------------------------
    function send_response() {
        $filecontent= json_encode($this->data);
        header("Content-Type: application/json; charset=UTF-8");
        echo $filecontent;
    }
    //-------------------------------------------------------------

}
//=================================================================

?>
