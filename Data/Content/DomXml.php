<?php
 /**
  *
  * @package Data
  *
  */

 /**
  * @subpackage Formats
  */
//=================================================================

class DomXmlRead extends XmlRead {

//=================================================================

    //-------------------------------------------------------------
    function onMakeNode($cls,$root,$attributes) {
        $attributes["__class__"]=$cls;
        //echo $root->path()."\n";
        //show($attributes);
        try {
            //echo "  class       ".$cls."   ".$root->path()."\n";
            $node= new $cls($root,$attributes);
        }
        catch (Error $e) {
            //echo "X class error ".$cls."   ".$root->path()."\n";
            $node= new TreeNode($root,$attributes);
            //$node->set("text",$e->getMessage());
        }
        return $node;
    }

    //-------------------------------------------------------------

}
//=================================================================
?>
