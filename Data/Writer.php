<?php

 /**
  *
  * @package Interface
  *
  */

 /**
  *
  * @subpackage Data
  *
  */
//=================================================================
class Writer extends TreeNode {
//=================================================================

    //-------------------------------------------------------------
    function writefile($url,$node) {
        $data=$this->onSerializeNode($node);
        return  $this->onWriteFile($url,$data);

    }
    //-------------------------------------------------------------
    function write($node) {
        $data=$this->onSerializeNode($node);
        return  $this->onWrite($data);

    }
    //-------------------------------------------------------------
    function onWriteFile($url,$data) {
        return ;
    }
    //-------------------------------------------------------------
    function onWrite($data) {

        return ;
    }
    //-------------------------------------------------------------
    function onSerializeNode($node) {
        return $node;

    }
    //-------------------------------------------------------------
    function onMakeNode($cls,$root,$attributes) {

            $node= new TreeNode($root,$attributes);
            $node->data["__class__"]=$cls;
            return $node;
    }
    //-------------------------------------------------------------

}
//=================================================================
?>
