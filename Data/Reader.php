<?php
 /**
  *
  * @package Interface
  *
  */

 /**
  *
  * @subpackage Data
  *
  */

//=================================================================
class Reader extends TreeNode {
//=================================================================

    //-------------------------------------------------------------
    function readfile($url,$root=null) {

        if (file_exists($url)) {
            $data = $this->onReadFile($url);
            //new TreeNode($this,["name"=>"load","text"=>$url]);
            return $this->onDeserializeNode($data,$root);
        } else {
            echo "error : not exists ".$url;
            exit();
        }



    }
    //-------------------------------------------------------------
    function read($string,$root=null) {
        $data = $this->onRead($string);
        return $this->onDeserializeNode($data,$root);

    }

    //-------------------------------------------------------------
    function onReadFile($url) {
        return $url;
    }
    //-------------------------------------------------------------
    function onRead($data) {

        return;
    }
    //-------------------------------------------------------------
    function onDeserializeNode($data,$root=null) {

        return;
    }
    //-------------------------------------------------------------


}
//=================================================================
?>
