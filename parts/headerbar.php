
<?php
$nav_bar=new NavbarView();
new NavbarBrand($nav_bar,["name"=>$application->get("name"),"url"=>"/","icon"=>$application->front_image()]);

foreach($application->all()->by_class("Plugin") as $plugin){
    $plugin_item=new NavbarDropdown($nav_bar,["name"=>$plugin->get("name")]);


        foreach($plugin->all()->by_class("PluginPage") as $page){
            new NavbarItem($plugin_item,["name"=>$page->get("name"),"url"=>$API_URL."?path=".$page->get_path()]);
    }
    
}
//$plugin_item=new NavbarDropdown($nav_bar,["name"=>"code source"]);
//new NavbarItem($plugin_item,["name"=>"code source","url"=>"https://framagit.org/GwikGwik/visites3d"]);
//new NavbarItem($plugin_item,["name"=>"wiki","url"=>"https://framagit.org/GwikGwik/visites3d/-/wikis/home"]);
//new NavbarItem($plugin_item,["name"=>"discussion","url"=>"https://nx.anadenn.fr/call/fxtb6um8"]);

$nav_bar->build();
?>

