<?php
 /**
  *
  * @package Interface
*
*/

$DATA_ID=0;
 /**
  *
  * @subpackage Program
  *
  */
//-------------------------------------------------------------
function get_new_id(){
//-------------------------------------------------------------
    global $DATA_ID;
    $name="CONTENT_".$DATA_ID;
    $DATA_ID+=1;
    return $name;
}
//-------------------------------------------------------------
 /**
  *
  * @subpackage Program
  *
  */
//=================================================================
class View extends Action {
//=================================================================

    public $content_id;

    //-------------------------------------------------------------
    function onInit() {
        parent::onInit();
        $this->content_id=get_new_id();
    }
    //-------------------------------------------------------------

    function onPreCall($request){}
    function onPostCall($request){}

    function onPreChildren($node,$request){}
    function onPreChild($action,$node,$request){}
    function onPostChild($action,$node,$request){}
    function onPostChildren($node,$request){}
    //-------------------------------------------------------------
    function do($node){
        $request=[];
        $this->onPreCall($request);

        //echo $this->get("select")."----";
        foreach($node->search($this->get("select",""))->iter() as $elt){
            //echo $elt->path();
            $data=new BaseClass($this);        
            $data->convert_from_node($elt);
            $this->build_node($elt,$data);
        }
        $this->onPostCall($request);
    }
    //-------------------------------------------------------------
    function build_node($node,$request){
            //echo $node->path();
            $this->onPreChildren($node,$request);
            foreach($this->children->by_class("Action")->iter() as $action){

                if($action->exists("select") && $node->search($action->get("select"))->is_empty()){
                }else{
                    $action->selected=$node;
                    $this->onPreChild($action,$node,$request);
                    $action->selected=$node;
                    $action->do($node,$request);
                    $this->onPostChild($action,$node,$request);
                }

            }
            $this->onPostChildren($node,$request);
    }

    //-------------------------------------------------------------
    function get_filename(){

        if($this->parent){
            return $this->root()->get_filename();
        }else{
            return "toto";
        }
    }

    //-------------------------------------------------------------
}

//=================================================================
?>
