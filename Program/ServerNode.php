<?php
 /**
  *
  * @package Interface
*
*/
 /**
  *
  * @subpackage Program
  *
  */
//=================================================================

function url($node) {

//=================================================================

    global  $MAIN_PATH;
    global  $MAIN_URL;
    global $API_URL;


    //echo $node->path();


    if($node instanceof Action || $node instanceof Method || $node instanceof ClassMethod || $node instanceof PluginPage){

        return $API_URL."?filename=".$node->root()->get_filename()."&start=".$node->path();

    }else if($node->exists("url")){
        return $node->get("url");

    }else if($node instanceof DiscNode){

        return str_replace($MAIN_PATH,"", $node->get("path"));//$MAIN_URL.

    }else {
        return $API_URL."?filename=".$node->root()->get_filename()."&start=/main&select=".$node->path();

    }

}
 /**
  *
  * @subpackage Program
  *
  */
//=================================================================

function url_action($node,$action){

//=================================================================
//echo url($action);
    return url($action)."&select=".$node->path();

}
 /**
  *
  * @subpackage Program
  *
  */
//=================================================================

function icon($node) {

//=================================================================

    global  $STATIC_DIRECTORY;
    if($node->exists("icon")){
            $image_url=$node->get("icon");
     }else if($node instanceof DiscNode){
        if(is_file($node->get("path")."/index.jpg")){
            $image_url=url($node)."/index.jpg";
        }else if(is_file($node->get("path"))){
            $image_url=url($node)."/index.jpg";
        }else if(is_file($node->get("path")."/index.png")){
            $image_url=url($node)."/index.png";
        }else if($node->parent){
            $image_url=icon($node->parent);
        }else{
            $image_url=$STATIC_DIRECTORY."/php/default.jpg";
        }
    }else{
        if(is_file($STATIC_DIRECTORY."/icons/".get_class($node).".jpg")){
            $image_url=$STATIC_DIRECTORY.url($node)."/index.jpg";
        }else{
            $image_url=$STATIC_DIRECTORY."/php/default.jpg";
        }
    }
    return $image_url;
}
 /**
  *
  * @subpackage Program
  *
  */
//=================================================================

function background($node) {

//=================================================================

    global  $STATIC_DIRECTORY;
    //echo $this->url()."XXXXXXXXXXX".$this->get("path");
    if($node->exists("background") && is_file($node->get("background"))){
        $image_url=$node->get("background");

    }else if($node->exists("path") && is_file($node->get("path")."/background.jpg")){
        $image_url=url($node)."/background.jpg";

    }else if($node->exists("path") && is_file($node->get("path")."/background.png")){
        $image_url=url($node)."/background.png";

    }else  if($node->parent){
        $image_url=background($node->parent);

    }else{
        $image_url=$STATIC_DIRECTORY."/blackboard/background.jpg";
    }
    return $image_url;
}
 /**
  *
  * @subpackage Program
  *
  */
//=================================================================

function url_plugin($node,$path){

//=================================================================

    global $MAIN_PATH;
    global $API_URL;
    //var_dump($path);
    if(is_string($path)){
        //echo $path;

        $plugin=$node->find($path);

    }else{
        $plugin=$path;
        $path=$plugin->path();
    }

    //show($plugin);
     if($plugin instanceof DiscNode){
        if ($plugin && $plugin->has_file("index.php")) {

            return str_replace($MAIN_PATH,"", $plugin->get("path"))."/index.php?&start=/main&select=".$node->path();
        }elseif ($plugin && $plugin->has_file("index.xml")) {

            return $API_URL."?filename=".url($plugin)."/index.xml&start=/main&select=".$node->path();
        }else{
            echo "no plugin".$path;
        }
    }

}

 /**
  *
  * @subpackage Program
  *
  */
//=================================================================

class ServerNode extends DiscNode {

//=================================================================

}
 /**
  *
  * @subpackage Program
  *
  */
//=================================================================

class Main extends ServerNode {

//=================================================================
    public $request;

    //-------------------------------------------------------------
    function get_filename(){
        global  $MAIN_PATH;
        return str_replace($MAIN_PATH,"", $this->get("filename"));

    }
    //-------------------------------------------------------------
    function onPostSetup(){
        parent::onPostSetup();
        $this->set("name","root");

        $this->request=new BaseClass();
        $this->onParseArgs($this->request);
        $this->parse_request($this->request);

        $filename=$this->get("filename");
        //echo $filename;
        $xml_parser=new DomXmlRead();
        $xml_parser->readfile($filename,$this);


    }
    //-------------------------------------------------------------
    function onParseArgs($request){

    }
    //-------------------------------------------------------------
    function parse_request($request){

        global $MAIN_PATH;

        $this->set("filename",$MAIN_PATH.$request->get("filename","./main.xml"));
        $this->set("start",$request->get("start","/main"));
        $this->set("page",$request->get("page","included"));//"included"
        $this->set("select",$request->get("select","/"));
    }
    //-------------------------------------------------------------
    function call($request=[]){


        $start=$this->get("start");
        $select=$this->get("select");
        $page=$this->get("page");

        $nodes=$this->search($select);
        if($nodes->is_empty() ){
            echo "no node ".$select;
            exit();
        }
        //show($this);
        $actions=$this->search($start);
        if($actions->is_empty() ){
            echo "no action ".$start;
            exit();
        }
        //show($actions);
        if( $page=="included" ){
            $this->header($actions->first(),$nodes->first());
        }

        foreach($nodes->iter() as $node){
            foreach($actions->iter() as $action){

                if ($action instanceof Action){
                    //echo $action->path()."\n";
                    $action->do($node,$request);

                }else if ($action instanceof ScriptNode){
                    //echo $action->path()."\n";
                    $action->selected=$node;
                    $action->call($request);

                }else if ($action instanceof PluginPage){
                    $action->call($request);
                }
            }
        }

        if( $page=="included" ){
            $this->footer($this);
        }

    }
    //-------------------------------------------------------------
    function header($action,$node) {
    global $GLOBALS;
        global $PART_DIRECTORY;
        global $STATIC_DIRECTORY;
        include($PART_DIRECTORY."/head.php");

    }
    //-------------------------------------------------------------
    function footer($node) {
    global $GLOBALS;
        global $PART_DIRECTORY;
        global $STATIC_DIRECTORY;
        include($PART_DIRECTORY."/footer.php");

    }
    //-------------------------------------------------------------
}
 /**
  *
  * @subpackage Program
  *
  */
#=================================================================

class Import extends Action {

#=================================================================

    //-------------------------------------------------------------
    function onInit(){
        parent::onInit();
        $this->set("event","init");
    }
    //-------------------------------------------------------------
    function onDo($node,$data){
        global $MAIN_PATH;
        $dom= new DomXmlRead();

        if( $this->get("url")[0] !="." ){
            $result=$dom->readfile($MAIN_PATH.$this->get("url"),$node);
        }else{
            $result=$dom->readfile($this->get("url"),$node);
        }

        //tree($result);

        if($this->get('import')=="children"){
            foreach( $result->children->iter() as $child){
                $child->attach($node);
                if($child instanceof Action && $child->get("event")=="init") {
                    $child->do($node,$data);
                }
            }
        }
    }
    //-------------------------------------------------------------
}
 /**
  *
  * @subpackage Program
  *
  */
#=================================================================

class Method extends Action {

#=================================================================

    function onInit(){
        parent::onInit();
        $this->set("event","command");
    }
    //-------------------------------------------------------------
    function onDo($node,$data){
        $this->search("*/[Action]/[event=call]")->execute("do",$node,$data);
    }
    //-------------------------------------------------------------
}
#=================================================================
?>
