<?php
 /**
  *
  * @package Interface
*
*/
 /**
  *
  * @subpackage Program
  *
  */
#===================================================================

class Command extends Action {

#===================================================================

    //-------------------------------------------------------------
    function __str__() {
        return $this->get("__class__")." - ".$this->get("text");
    }
    //-------------------------------------------------------------
    function onDo($node,$data){

            $local_path=$data->get("path",".");
            $command=$data->get("text","ls");
            $command1="cd ".$local_path." && ".$command." 2>&1";

            $result=null;
            $output=null;
            $retval=null;
            //echo $local_path;

            exec($command1, $output, $retval);

            if($retval==0){
                ?>
                <h5 class="card success"><?=$command?> -  <?=$local_path?></h5>
                <pre class="infos">
                <?php

                foreach ($output as $value) {
                    echo $value."\n";
                }
            }else{
                 ?>
                <h5 class="frame warning"><?=$command?>-  <?=$local_path?></h5>
                <pre class="infos">
                <?php
                echo $retval;
                foreach ($output as $value) {
                    echo $value."\n";
                }
            }

            echo "</pre>";
    }
    //-------------------------------------------------------------

}


//=====================================================
?>
