<?php
#=================================================================
/**
* executer une action sur un autre noeud
*
*   * select : filtre pour sélectionner les données
*   * filter : filtre sur les selecteurs enfants
*
* @package Interface
*
*/


 /**
  *
  * @subpackage Program
  *
  */
#=================================================================

class Data extends TreeWorkflow {

#=================================================================

    function onPreSetup(){


        //show($this);
    }


    function onPreCleanup(){
        $this->call_event("cleanup");
    }

    function onPostCleanup(){
        $this->call_event("destroy");
    }




}
 /**
  *
  * @subpackage Program
  *
  */
#=================================================================

class ScriptNode extends TreeSelect {

#=================================================================

    //public $globals= null;

    function onPreSetupX(){
        $this->set("select_children","*/[ScriptNode]");
        $this->call_event("init");
        //show($this->globals);
        $this->convert_globals();
        $this->search("*/[EventHandler]")->execute("convert_globals");
        $this->call_event("setup");
        //show($this);
    }


    function onPreChildren($node,$request){
        $this->call_event("call",$node);
    }

    function onPreCleanup(){
        $this->call_event("cleanup");
    }

    function onPostCleanup(){
        $this->call_event("destroy");
    }



    function onPreCall($request=[]){}
    function onPostCall($request=[]){}
    function onPostChildren($node,$request){}


    //-------------------------------------------------------------

}
 /**
  *
  * @subpackage Program
  *
  */
#=================================================================
class Action extends EventHandler {

#=================================================================
    function onInit(){
        parent::onInit();
        $this->set("event",$this->get("event","call"));
    }

    function onDo($node,$data){
        $this->search("*/[Action]/[event=call]")->execute("do",$node,$data);
    }

    //-------------------------------------------------------------

}
 /**
  *
  * @subpackage Program
  *
  */
#=================================================================

class GlobalVariable extends Action {

#=================================================================
    function onInit(){
        parent::onInit();
        $this->set("event","init");
    }

    function onDo($node,$data){
        $this->root()->set_global($this->get("name"),$this->get("text"));
    }
}
 /**
  *
  * @subpackage Program
  *
  */
#=================================================================

class PhpGlobal extends GlobalVariable {

#=================================================================
    function onInit(){
        global $GLOBALS;
        parent::onInit();


        if($this->exists("dict") && ! array_key_exists($this->get("dict"),$GLOBALS)  ){
            $this->set("text","XXXXXXXXXX");
            return;
        }else if($this->exists("dict") && array_key_exists($this->get("dict"),$GLOBALS) ){
            $dict=$GLOBALS[$this->get("dict")];
        }else{
            $dict=$GLOBALS;
        }

        if(array_key_exists($this->get("name"),$dict)  ){
            $this->set("text",$dict[$this->get("name")]);
        }else{
            $this->set("text","XXXXXXXXXXXXX");
        }
    }


}
 /**
  *
  * @subpackage Program
  *
  */
#=================================================================

class Call extends Action {

#=================================================================

    function onDo($node,$data){
       foreach($node->search($this->get("method",""))->iter() as $elt){

            if ($elt instanceof Action){
                $elt->selected=$node;
                $elt->do($node,$data);
            }else if ($elt instanceof ScriptNode){
                $elt->selected=$node;
                $elt->call();
            }
        }
    }
}
#=================================================================


?>
