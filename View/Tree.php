<?php
 /**
  *
  * @package View
  *
  */


 
/**
  * @subpackage utils
  */
function plot_tree($node,$max=2,$level=0){

if($level >= $max){
return;
}
?>
<style>
.treelist{
   list-style-type: none;
Xborder-top:1px solid grey;
Xborder-left:1px solid grey;
border:1px solid cadetblue;
margin-left:20px;
padding-bottom:20px;
Xpadding-top:20px;
width:100%;
max-width:800px;
}

.treelist li{
max-width:100%;
}
.treetable{
Xborder-top:1px solid grey;
Xborder-bottom:1px solid grey;
max-width:15vw;
}
.treetable th,.treetable td{
padding:3px;
}
.treetable td{
Xborder-bottom:1px solid grey;
}
</style>
    <ul class="treelist">
    <div><?=$node->get("text")?></div>
<div><?=get_class($node)?> <span style="color:grey"><?=$node->get_path()?></span></div>

<table class="w3-table w3-small treetable" style="">

        <?php 
        foreach($node->keys() as $k){
            if($k!="name" && $k!="text"){

        ?>
           <tr><th><?=$k?></td><td> <?=$node->get($k)?></td></tr>
        <?php
        }
        }
        ?>
 </table>
        <?php 
        foreach($node->children->iter() as $child){
        ?>
            <li><?=plot_tree($child,$max,$level+1)?></li>
        <?php
        }
        ?>
    </ul>
<?php
} /**
  * @subpackage Data
  */
//=================================================================
class Tree extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
        ?>

<style>

#api_content{
color:white;
    margin:50px;
    Xmargin-left:100px;
    Xmargin-right:100px;
background-color:rgba(0,0,0,0.9);
}

h1{
    margin-top:25px;
    margin-bottom:25px;
    border-bottom:1px solid black;
}

h2{
    background-color:grey;
}
</style>
<div id="api_content">
<?php
plot_tree($node,2);

?>

</div>
        <?php

    }
    //-------------------------------------------------------------
    function onPostChildren($node,$request) {
        ?>

        <?php
    }

    //-------------------------------------------------------------

}

//=================================================================
?>
