<?php
/**
*
* @package View
*
*/
//=================================================================
class Tabs extends View {
//=================================================================
    var $elements;
    //-------------------------------------------------------------
    function do($node) {
    //-------------------------------------------------------------

        //$current_selection=$root->search($this->get("select",""));

        $this->content_id=get_new_id();
        ?>
        <ul class="nav nav-tabs" id="<?=$this->content_id?>" role="tablist">
        <?php


        $i=0;
        $height=intval($this->get("h",100));
        foreach($this->search("*/[Action]")->iter() as $action){
            $this->onTabHead($node,$action,$i);
            $i+=1;
        }
        ?>
        </ul>
        <div class="tab-content" id="<?=$this->content_id?>_content" style="height:<?=$height?>vh;overflow-y:scroll;">
        <?php
        $i=0;
            foreach($this->search("*/[Action]")->iter() as $action){

                $this->onTabContent($node,$action,$i);
                $i+=1;
        }

        ?>
        </div>
        <?php
    }
    //-------------------------------------------------------------
    function onTabHead($node,$action,$i) {
    //-------------------------------------------------------------

            $action_id=get_new_id();
            $action->set("content_id",$action_id );
            $child_button_id=$this->content_id.$action_id."_button";
            $child_content_id=$this->content_id.$action_id."_content";
            if( $i == 0){
                $classes="nav-link active";
                $selected="true";
            }else{
                $classes="nav-link";
                $selected="false";
            }
            ?>
            <li class="nav-item" role="presentation">
            <button class="<?=$classes?>" 
                    id="<?=$child_button_id?>" 
                    data-bs-toggle="tab" 
                    data-bs-target="#<?=$child_content_id?>" 
                    type="button" 
                    role="tab" 
                    aria-controls="<?=$child_content_id?>" 
                    aria-selected="<?=$selected?>"><?=$action->get("name")?></button>
             </li>
            <?php


    }
    //-------------------------------------------------------------
    function onTabContent($node,$action,$i) {
    //-------------------------------------------------------------
            $action_id=$action->get("content_id" );
            $child_button_id=$this->content_id.$action_id."_button";
            $child_content_id=$this->content_id.$action_id."_content";
            if( $i == 0){
                $classes="tab-pane fade show active";
            }else{
                $classes="tab-pane fade";
            }

            ?>
            <div class="<?=$classes?>" 
                    id="<?=$child_content_id?>" 
                    role="tabpanel" 
                    aria-labelledby="<?=$child_button_id?>" 
                    tabindex="<?=$i?>">
            <?php

                $action->do($node);

                ?>
            </div>
            <?php


    }
    //-------------------------------------------------------------

}
//=================================================================
?>
