<?php
 /**
  *
  * @package View
  *
  */
//=================================================================
class Sidebar extends View {
//=================================================================
    var $id;

    //-------------------------------------------------------------
    function onPreCall($request) {
    //-------------------------------------------------------------
        $this->id=get_new_id();
        $height=intval($this->get("h",100));
        ?>
<script>
var id="<?=$this->id?>";
function w3_open() {
  document.getElementById(id).style.position = "fixed";
  document.getElementById(id).style.top = "0";
  document.getElementById(id).style.left = "0";
  document.getElementById(id).style.display = "block";
  document.getElementById(id).style.width = "100vw";
  document.getElementById(id).style.height = "100vh";
}

function w3_close() {
  document.getElementById(id).style.display = "none";
}
</script>
  <button class="w3-button w3-tealx w3-xlarge" onclick="w3_open()">☰</button>

<div class="w3-sidebar w3-bar-block w3-border-right MySideBar" style="background-color:rgba(0,0,0,0.95);display:none" id="<?=$this->id?>">
  <div onclick="w3_close()" classX="w3-bar-item w3-large">Close &times;</div>

        <?php
    }
    //-------------------------------------------------------------
    function onPostCall($request) {
    //-------------------------------------------------------------
        ?>
        </div>
        <?php
    }
    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
    //-------------------------------------------------------------

        ?>
                 <div classX="w3-bar-item w3-button">
        <?php

    }
    //-------------------------------------------------------------
    function onPostChildren($node,$request) {
    //-------------------------------------------------------------
        ?>
        </div>
        <?php
    }
    //-------------------------------------------------------------

}
?>
