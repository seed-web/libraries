<?php
 /**
  *
  * @package View
  *
  */


 /**
  *
  * @subpackage Navbar
  *
  */
//=================================================================
class Navbar  extends View {
//=================================================================

    //-------------------------------------------------------------
    function onPreCall($request) {
        ?>
        <nav class="navbar  navbar-expand-md ">
          <div class="container-fluid">
        <?php

    }
    //-------------------------------------------------------------
    function onPostCall($request) {
        ?>
          </div>
        </nav>
        <?php
    }


    //-------------------------------------------------------------

}

//=================================================================
class NavbarNav  extends View {
//=================================================================

    //-------------------------------------------------------------
    function onPreCall($request) {
        ?>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-nav-bar" aria-controls="main-nav-bar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="main-nav-bar">

              <ul class="navbar-nav">
        <?php

    }
    //-------------------------------------------------------------
    function onPostCall($request) {
        ?>
        </ul>
        </div>
        <?php
    }
    //-------------------------------------------------------------
    function onPreChild($action,$node,$request) {
    //-------------------------------------------------------------

        ?>
        <li class="nav-item">
        <?php
    }
    //-------------------------------------------------------------
    function onPostChild($action,$node,$request) {
    //-------------------------------------------------------------
        ?>
        </li>
        <?php

    }
}
 /**
  *
  * @subpackage Navbar
  *
  */
//=================================================================
class NavbarBrand  extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
        ?>
    <a class="navbar-brand" href="<?=url($node)?>">
      <img src="<?=icon($node)?>" alt="Logo" width="30" height="24" class="d-inline-block align-text-top">
      <?=$node->get("name")?>
    </a>
        <?php
    }

    //-------------------------------------------------------------
}
 /**
  *
  * @subpackage Navbar
  *
  */
//=================================================================
class NavLink  extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
        if($this->exists("text")){
            $text=$this->get("text");
        }else{
            $text=$node->get("name");
        }
        ?>
            <a class="nav-link dropdown-item" 
            href="<?=url($node)?>"><?=$text?></a>
        <?php
    }

    //-------------------------------------------------------------
}
 /**
  *
  * @subpackage Navbar
  *
  */
//=================================================================
class DropdownItem  extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
        if($this->exists("text")){
            $text=$this->get("text");
        }else{
            $text=$node->get("name");
        }
        ?>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <?=$text?>
          </a>
          <ul class="dropdown-menu bg-dark">

        <?php


    }
    //-------------------------------------------------------------
    function onPostChildren($node,$request) {
        ?>
          </ul>
        </li>
        <?php
    }
    //-------------------------------------------------------------
    function onPreChild($action,$node,$request) {
    //-------------------------------------------------------------

        ?>
<li class=" ">
        <?php
    }
    //-------------------------------------------------------------
    function onPostChild($action,$node,$request) {
    //-------------------------------------------------------------
        ?>
</li>
        <?php

    }
    //-------------------------------------------------------------
}
//=================================================================



