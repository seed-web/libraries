<?php
 /**
  *
  * @package View
  *
  */
//=================================================================
class XmlView extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
//echo $node->path();
//echo $this->get("method");
foreach( $node->search($this->get("method"))->iter() as $method){
//echo $method->path();
//echo url_action($node,$method);

//echo $method->path();
        $this->content_id=get_new_id();
        ?>
        <div  id="<?=$this->content_id?>"></div>
        <script type="module">
          import {Xml} from '/seed-env/__js/new/DataFormat/Xml.js';
          var doc=new Xml(null,{content_id:"#<?=$this->content_id?>",url:"<?=url_action($node,$method)?>&page=0"});
        </script>
        <?php
//echo $method->path();
}
    }
    //-------------------------------------------------------------

}
//=================================================================
class XmlView2 extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
        global $PLUGIN_URL;
        if( $node->exists("path") ){
            $data=$PLUGIN_URL."/js/data.php?path=".$node->get("path");
        }else{
            $data=$PLUGIN_URL."/js/data.php";
        }
        $theme=$this->get("theme","topmenu");
        foreach( $node->search($this->get("method"))->iter() as $method){

                $this->content_id=get_new_id();
        ?>
                <div class="card" id="<?=$this->content_id?>" style="height:100vh"></div>
                <script>
                DEBUG_LEVEL=3;

                    var root="/";
                    var path="/";

                    var main=new Bd_Main("<?=$this->content_id?>","/seed-env/__gui/<?=$theme?>/index.xml","<?=$data?>");


                function script(){
                    console.log("ok");
                    $("#main").empty();
                    main.run();

                };

                //setTimeout(script, 200);
                </script>
        <?php
        //echo $method->path();
        }
    }
    //-------------------------------------------------------------

}
//=================================================================
?>
