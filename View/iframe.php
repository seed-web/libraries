<?php
 /**
  *
  * @package View
  *
  */

 /**
  * @subpackage Links
  */
//=================================================================
class IFrame extends View {
//=================================================================
    var $id;
    //-------------------------------------------------------------
    //-------------------------------------------------------------
    function onPreCall($request) {
    //-------------------------------------------------------------
        $this->id=get_new_id();
        $height=intval($this->get("h",100));
        ?>
<script>
var id="#<?=$this->id?>";
function iframe_click(url){
console.log('ok');
    $(id).attr("src",url);
    $(id+"_link").attr("href",url);
}
</script>

        <div class="w3-row-padding">
            <div class="w3-col s12 m4 l2" style="background-color:rgba(0,0,0,0.5);height:<?=$height?>vh;" >
        <?php
    }
    //-------------------------------------------------------------
    function onPostCall($request) {
    //-------------------------------------------------------------
        $height=intval($this->get("h",100));
        $width=intval($this->get("w",50));
        ?>
        </div>
            <div class="w3-rest" style="height:<?=$height?>vh;">
                <a href="" id="<?=$this->id?>_link"><p>direct link</p></a>
                <iframe src="" id="<?=$this->id?>" style="height:<?=$height-10?>vh;width:<?=$width?>vw;background-color:rgba(0,0,0,0.5);margin:15px;padding:15px;border-radius:15px;"></iframe>
            </div>
        </div>
        <?php
    }
    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
    //-------------------------------------------------------------
        $action=$this->search($this->get("method",null))->first();
        $plugin=$this->search($this->get("plugin",null))->first();
        
        if ($node instanceof Service){
            $url=url($node);
        } else if ($action){
            $url=url_action($node,$action);
        } else if ($plugin){
            $url=url_plugin($node,$plugin);
        
        } else{
            $url=url($node);
        }
        ?>
                <li class="list-group-item list-group-item-action d-flex gap-3 py-3 card-items"  style="color:white;" onclick="iframe_click('<?=$url?>')"><?=$node->get("name")?></li>
        <?php

    }
    //-------------------------------------------------------------

}
 /**
  * @subpackage Links
  */
//=================================================================
class IFrame2 extends View {
//=================================================================
    var $id;


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
    //-------------------------------------------------------------
        $this->id=get_new_id();

        $height=intval($this->get("h",100));
        $action=$this->find($this->get("method",null));
        $plugin=$this->find($this->get("plugin",null));
        //tree($action);
        //tree($plugin);

        if ($action){
            $url=url_action($node,$action);
        } else if ($plugin){
            $url=url_plugin($node,$plugin);
        
        } else if ($node instanceof Service){
            $url=url($node);
        } else {
            $url=url($node);
        }
        //echo $url;
        ?>
                <iframe src="<?=$url?>" id="<?=$this->id?>" style="height:<?=$height?>vh;width:100%;">
                    </iframe>
        <?php

    }
    //-------------------------------------------------------------

}
//=================================================================



?>
