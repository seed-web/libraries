<?php
/**
*
* @package View
*
*/

 /**
  * @subpackage Cards
  */
/*function str_starts_with ( $haystack, $needle ) {
  return strpos( $haystack , $needle ) === 0;
}*/


 /**
  * @subpackage Cards
  */
//=================================================================
class CardHeader extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
    //-------------------------------------------------------------
        $height=intval($this->get("h",10));
        if($this->exists("icon")){
            $icon=$this->get("icon");
        }else {
            $icon=icon($node);
        }

        $text="";
        if($node->parent){
            $text=$node->parent->get("name");
        }
        ?>

              <div class="card-header">

<div  class="card-title">
<div  class="card-img">
                  <img src="<?=$icon?>" alt="twbs" class="rounded-circle flex-shrink-0">
</div>
                 <h5>
                        <?=$node->get("name")?>
                </h5>    
                <p><?=get_class($node)?> - <?=$text?></p> 
</div>
 <div class="image-container">
            <img class="card-img-top img-fluid .hover-zoom"  style="height:<?=$height?>vh;"  src="<?=background($node)?>"/>

  <div class="image-text-centered"><?=$node->get("text")?></div>
</div> 
        <?php

    }
    //-------------------------------------------------------------

    function onPostChildren($node,$request) {
        ?>
              </div>
        <?php
    }
    //-------------------------------------------------------------


}

 /**
  * @subpackage Cards
  */
//=================================================================
class CardHeaderShort extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
    //-------------------------------------------------------------
        $height=intval($this->get("h",10));
        if($this->exists("icon")){
            $icon=$this->get("icon");
        }else {
            $icon=icon($node);
        }


        ?>

              <div class="card-header">

<div  class="card-title">
<div  class="card-img">
                  <img src="<?=$icon?>" alt="twbs" class="rounded-circle flex-shrink-0">
</div>
                 <h5  style="font-size:14px;">
                        <?=$node->get("name")?>
                </h5>    
                <p><?=get_class($node)?> </p> 
</div>


                

 <div class="image-container">
            <img class="card-img-top img-fluid .hover-zoom"  style="height:<?=$height?>vh;"  src="<?=background($node)?>"/>

  <div class="image-text-centered"><?=$node->get("text")?></div>
</div> 

        <?php

    }
    //-------------------------------------------------------------

    function onPostChildren($node,$request) {
        ?>
              </div>
        <?php
    }
    //-------------------------------------------------------------


}

 /**
  * @subpackage Cards
  */
//=================================================================
class CardBody extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
    //-------------------------------------------------------------

        ?>

              <div class="card-body scrollable">

        <?php

    }
    //-------------------------------------------------------------

    function onPostChildren($node,$request) {
        ?>
              </div>
        <?php
    }
    //-------------------------------------------------------------


}

 /**
  * @subpackage Cards
  */
//=================================================================
class CardFooter extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
    //-------------------------------------------------------------

        ?>

  <div class="card-footer text-muted">
                <p style="color:grey;"><?=$node->path()?></p> 

        <?php

    }
    //-------------------------------------------------------------

    function onPostChildren($node,$request) {
        ?>
              </div>
        <?php
    }
    //-------------------------------------------------------------


}

 /**
  * @subpackage Cards
  */
//=================================================================
class Card extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
    //-------------------------------------------------------------

        global $API_URL;
        $id=get_new_id();
        $height=intval($this->get("h",60));
        ?>

        <div class="card" style="height:<?=$height?>vh;" id="<?=$id?>">

        <?php

    }
    //-------------------------------------------------------------

    function onPostChildren($node,$request) {
        ?>
          </div>
        <?php
    }
    //-------------------------------------------------------------


}

//=================================================================

?>
