<?php
 /**
  *
  * @package View
  *
  */

 /**
  * @subpackage Links
  */
#===================================================================

class PluginLink extends View {

#===================================================================
    //-------------------------------------------------------------
    function build_node($node,$request){

        foreach($this->search($this->get("plugin","/**/[Plugin]"))->iter() as $service){//
            $this->onPreChild($service,$node,$request);
            foreach($this->children->by_class("Action")->iter() as $action){

                if($this->get("select_node",null)){
                    $action->do($node,$request);                
                }else{
                    $action->do($service,$request);
                }
            }
            $this->onPostChild($service,$node,$request);
        }
    }
    //-------------------------------------------------------------
    function onDoX($node,$request) {
    //-------------------------------------------------------------

        //$this->search($this->get("service","/**/[Plugin]"))->plot();
        foreach($this->search($this->get("plugin",""))->iter() as $service){///**/[Plugin]
            //echo "<p>run service".$service->path()."</p>";
            $this->onPreChild($service,$node,$request);
            foreach($this->search("*/[Action]") as $action){

                $action->do($service);//,$request);
            }
            $this->onPostChild($service,$node,$request);
        }


    }
    //-------------------------------------------------------------
    function onPreChild($service,$node,$request) {
    //-------------------------------------------------------------

        ?>
        <a class="Link" href="<?=url_plugin($node,$service)?>">
        <?php
    }
    //-------------------------------------------------------------
    function onPostChild($service,$node,$request) {
    //-------------------------------------------------------------
        ?>
        </a>
        <?php

    }

    //-------------------------------------------------------------

}
 /**
  * @subpackage Links
  */
#===================================================================

class ApiLink extends View {

#===================================================================

    //-------------------------------------------------------------
    function onPreChildren($node,$request) {

        $method=$this->find($this->get("method"));
        //echo url_action($node,$method);
        //echo url($method);
            ?>
            <a classX="nav-link dropdown-item Link"  href="<?=url_action($node,$method)?>">
            <?php

    }
    //-------------------------------------------------------------
    function onPostChildren($node,$request) {
        ?>
       </a>
        <?php
    }

    //-------------------------------------------------------------

}
 /**
  * @subpackage Links
  */
#===================================================================

class CallApiList extends View {

#===================================================================

    //-------------------------------------------------------------
    function build_node($node,$request){
    //-------------------------------------------------------------

            foreach($this->search($this->get("method"))->iter() as $service){
                $this->onPreChild($service,$node,$request);
                foreach($this->search("*/[Action]")->iter() as $action){
                    $action->do($service,$request);
                }
                $this->onPostChild($service,$node,$request);
            }

    }
    //-------------------------------------------------------------
    function onPreChild($service,$node,$request) {
    //-------------------------------------------------------------

        ?>
            <a class="nav-link dropdown-item Link"  href="<?=url_action($node,$service)?>">
        <?php
    }
    //-------------------------------------------------------------
    function onPostChild($service,$node,$request) {
    //-------------------------------------------------------------
        ?>
        </a>
        <?php

    }

    //-------------------------------------------------------------

}
 /**
  * @subpackage Links
  */
#===================================================================

class CallPlugin extends View {

#===================================================================

    //-------------------------------------------------------------
    function onPreChildren($node,$request) {


        $url=url_plugin($node,$this->get("plugin"));

            ?>
            <a class="nav-link dropdown-item Link"  href="<?=$url?>">
            <?php

    }
    //-------------------------------------------------------------
    function onPostChildren($node,$request) {
        ?>
       </a>
        <?php
    }

    //-------------------------------------------------------------


}

 /**
  * @subpackage Links
  */
#===================================================================

class DirectLink extends View {

#===================================================================

    //-------------------------------------------------------------
    function onPreChildren($node,$request) {

        ?>
        <a href="<?=url($node)?>">
        <?php
    }
    //-------------------------------------------------------------
    function onPostChildren($node,$request) {
        ?>
        </a>
        <?php
    }
    //-------------------------------------------------------------

}
 /**
  *
  * @package View
  *
  */

//=================================================================
class TreeIcon extends View {
//=================================================================

    //-------------------------------------------------------------
    function onPreChildren($node,$request) {

        if($this->exists("label")){
            $label=$this->get("label");
        }else {

            $label=$node->get("name");
        }

        if($this->exists("icon")){
            $icon=$this->get("icon");
        }else {
            $icon=icon($node);
        }


        if ($this->exists("text")){
        $text=$this->get("text");
        }else if($node->exists("text") && $node->get("text")!=""){
        $text=$node->get("text");
        }else{
        $text=get_class($node);
        }
        ?>

        <?php
        if($icon!=""){
        ?>
            <img src="<?=$icon?>" alt="twbs" width="50" height="50" class="rounded-circle flex-shrink-0">
        <?php
        }
        ?>
           


        <?php
    }
    //-------------------------------------------------------------

}
#===================================================================
 /**
  *
  * @package View
  *
  */

//=================================================================
class TreeButton extends View {
//=================================================================

    //-------------------------------------------------------------
    function onPreChildren($node,$request) {

        if($this->exists("label")){
            $label=$this->get("label");
        }else {

            $label=$node->get("name");
        }

        if($this->exists("icon")){
            $icon=$this->get("icon");
        }else {
            $icon=icon($node);
        }
//echo $node->path();

        if ($this->exists("text")){
        $text=$this->get("text");
        }else if($node->exists("text") && $node->get("text")!=""){
        $text=$node->get("text");
        }else if(! $node instanceof Action){
        $text=get_class($node);
        }else{
        $text="";
        }
        ?>
          <div class="list-group-item list-group-item-action d-flex gap-3 py-3 card-items" aria-current="true">
        <?php
        if($icon!=""){
        ?>
            <img src="<?=$icon?>" alt="no icon" width="32" height="32" class="rounded-circle flex-shrink-0">
        <?php
        }
        ?>

            <div class="d-flex gap-2 w-100 justify-content-between">
              <div>
                <h6 class="mb-0"><?=$label?></h6>

                <p class="mb-0 opacity-75"><?=$text?></p>

              </div>
            </div>
          </div>

        <?php
    }
    //-------------------------------------------------------------

}
 /**
  * @subpackage Links
  */
//=================================================================
class List_Item extends View {
//=================================================================

    //-------------------------------------------------------------
    function onPreCall($request) {
        ?>

<div class="list-group w-auto">
        <?php

    }
    //-------------------------------------------------------------
    function onPostCall($request) {
        ?>
        </div>
        <?php
    }
    //-------------------------------------------------------------

    function onPreChildren($node,$request) {
        $action=$this->get("method",null);
        if ($action){
            $url=url_action($node,$action);
        } else{
            $url=url($node);
        }
?>

  <a href="<?=url_action($node,$url)?>" class="list-group-item list-group-item-action d-flex gap-3 py-3 card-items" aria-current="true">
    <img src="<?=icon($node)?>" alt="twbs" width="32" height="32" class="rounded-circle flex-shrink-0">
    <div class="d-flex gap-2 w-100 justify-content-between">
      <div>
        <h6 class="mb-0"><?=$node->get("name")?></h6>
        <p class="mb-0 opacity-75"><?=$node->get("text")?></p>
      </div>
    </div>
  </a>

<?php




    }
    //-------------------------------------------------------------

}
 /**
  * @subpackage Links
  */
//=================================================================
class NodeActionsLink extends View {
//=================================================================

    //-------------------------------------------------------------
    function onPreCall($request) {
        ?>

          <ul class=" list-group list-group-flush ">
        <?php

    }
    //-------------------------------------------------------------
    function onPostCall($request) {
        ?>
          </ul>
        <?php
    }
    //-------------------------------------------------------------

    function onPreChildren($node,$request) {

        global $API_URL;

         //echo "<p>".$node->get("name")."<p>";
        $classes=$this->find("/classes");

            foreach($node->children->by_class("Action") as $action){
                //echo $action->get("name");
                    ?>
                    <a href="<?=url_action($node,$action->path())?>">
                        <li class="list-group-item list-group-item-action d-flex gap-3 py-3 card-items"><?=$action->get("name")?></li>
                    </a>
                    <?php

            }


    }
}
 /**
  * @subpackage Links
  */
//=================================================================
class NodeMethodList extends View {
//=================================================================

    //-------------------------------------------------------------
    function onPreCall($request) {
        ?>

          <ul class=" list-group list-group-flush ">
        <?php

    }
    //-------------------------------------------------------------
    function onPostCall($request) {
        ?>
          </ul>
        <?php
    }
    //-------------------------------------------------------------

    function onPreChildren($node,$request) {

        $tag=$this->get("tag","project");

         //echo "<p>".$node->get("name")."<p>";
        $classes=$this->find("/classes");
        foreach($classes->methods($node,$tag."_") as $action){

                $name=str_replace($tag."_","", $action->get("name"));
                ?>
                <a href="<?=url_action($node,$action)?>">
                    <li class="list-group-item list-group-item-action d-flex gap-3 py-3 card-items"><?=$name?></li>
                </a>
                <?php
        }

    }
    //-------------------------------------------------------------

}
 /**
  * @package todo
  */
//=================================================================
class CardService extends View {
//=================================================================

    //-------------------------------------------------------------
    function onPreCall($request) {
        ?>

<div class="list-group w-auto">
        <?php

    }
    //-------------------------------------------------------------
    function onPostCall($request) {
        ?>
        </div>
        <?php
    }
    //-------------------------------------------------------------

    function onPreChildren($node,$request) {
        global $API_URL;
?>

  <a href="<?=url($node)?>" class="list-group-item list-group-item-action d-flex gap-3 py-3 card-items" aria-current="true">
    <img src="<?=icon($node)?>" alt="twbs" width="32" height="32" class="rounded-circle flex-shrink-0">
    <div class="d-flex gap-2 w-100 justify-content-between">
      <div>
        <h6 class="mb-0"><?=$node->get("name")?></h6>
        <p class="mb-0 opacity-75"><?=$node->get("text")?></p>
      </div>
    </div>
  </a>

<?php




    }
    //-------------------------------------------------------------

}

 /**
  * @package todo
  */
//=================================================================
class CardButton extends View {
//=================================================================

    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
        ?>
            <a class="btn btn-primary" href="<?=$this->get("url")?>"><?=$this->get("name")?></a>
        <?php
    }
    //-------------------------------------------------------------

}
 /**
  * @package todo
  */
//=================================================================
class ButtonGroup extends View {
//=================================================================

    //-------------------------------------------------------------
    function onPreCall($request) {
?>
<div class="list-group w-auto">

<?php
    }
    //-------------------------------------------------------------
    function onPostCall($request) {
?>
   </div>
<?php
    }
    //-------------------------------------------------------------

}

//=================================================================
?>
