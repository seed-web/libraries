<?php

 /**
  *
  * @package View
  *
  */
#=================================================================

class Markdown extends View {

#=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {

        $url=$node->get("path");
        //echo $url;

        $output = shell_exec("pandoc -f markdown -t html $url");
        echo $output;
    }
    //-------------------------------------------------------------
}
#=================================================================
 /**
  *
  * @package View
  *
  */
#=================================================================

class Gallery extends View {

#=================================================================


    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
        //show($node);

        $mode=$this->get("mode","image");
        ?>
        <p style="font-size:10px;color:grey;"><?=$node->get("name")?></p>
        <?php
        if($mode == "content" && $node->is_content()){
        ?>
            <img src="<?=url($node)?>"/>
        <?php
        }elseif($mode == "image" && $node->is_image()){
        ?>
            <img src="<?=url($node)?>" style="background-color:grey;"/>
        <?php
        }elseif($mode == "embeded" && $node->is_embeded()){
        ?>
            <embed src="<?=url($node)?>" style="background-color:grey;font-color:white;height:100%;"/>
        <?php
        }elseif($mode == "video" && $node->is_video()){
        ?>
            <video src="<?=url($node)?>" controls/>
        <?php
        }else if($mode == "audio" && $node->is_audio()){
        ?>

            <audio src="<?=url($node)?>" controls/>
        <?php
        }


    }
    //-------------------------------------------------------------
    function make_readme($node,$data){

        $url=url($node);

            $output = shell_exec("pandoc $url -f markdown -t html");
            echo $output;
    }

}
#=================================================================

class FileIcon extends Gallery {

#=================================================================

    //-------------------------------------------------------------
    function onPreChildren($node,$request) {
        global $STATIC_DIRECTORY;
        //show($node);
        if($this->is_video($node)){
        ?>
            <img src="<?=$STATIC_DIRECTORY?>/blackboard/video.png"/>
        <?php
        }elseif($this->is_image($node)){
        ?>
            <img src="<?=url($node)?>"/>
        <?php
        }else{
        ?>
            <?=$node->get("name")?>
        <?php
        }
    }
    //-------------------------------------------------------------

}


?>
