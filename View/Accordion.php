<?php
 /**
  *
  * @package View
  *
  */
//=================================================================
class Accordion extends View {
//=================================================================


    //-------------------------------------------------------------
    function onPreCall($request) {


        ?>
        <div class="accordion" id="<?=$this->content_id?>">
        <?php
    }
    //-------------------------------------------------------------
    function onPreChild($method,$node,$request) {
        $this->content_id=get_new_id();
        $child_button_id=$this->content_id."_button";
        $child_content_id=$this->content_id."_content";
        ?>
          <div class="accordion-item">
            <h2 class="accordion-header" id="<?=$child_button_id?>">
              <button class="accordion-button collapsed" 
                        type="button" 
                        data-bs-toggle="collapse" 
                        data-bs-target="#<?=$child_content_id?>" 
                        aria-expanded="false" 
                        aria-controls="<?=$child_content_id?>">
                <?=$method->get("name")?>
              </button>
            </h2>
            <div id="<?=$child_content_id?>" 
                    class="accordion-collapse collapse" 
                    aria-labelledby="<?=$child_button_id?>" 
                    data-bs-parent="#<?=$this->content_id?>">
              <div class="accordion-body">


        <?php
    }
    //-------------------------------------------------------------
    function onPostChild($method,$node,$request) {
    //-------------------------------------------------------------
        ?>
              </div>
            </div>
          </div>
        <?php

    }
    //-------------------------------------------------------------

    function onPostCall($request) {
        ?>
        </div>
        <?php
    }

    //-------------------------------------------------------------

}
//=================================================================
?>
