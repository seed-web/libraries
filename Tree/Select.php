<?php
#=================================================================
/**
* executer une action sur un autre noeud
*
*   * select : filtre pour sélectionner les données
*   * filter : filtre sur les selecteurs enfants
*
* @package Interface
*
*/
#=================================================================

class Test extends TreeNode {

#=================================================================
    //-------------------------------------------------------------
    function call($node) {
        if($this->exists('method')){
            $method=$this->get('method');
            return $node->$method();
        }else{
            return true;
        }
    }
    //-------------------------------------------------------------
}
#=================================================================

class TreeSelect extends TreeRun {

#=================================================================
    public $selected;


    function onPreCall($request=[]){}
    function onPostCall($request=[]){}
    function onPreChildren($node,$request){}
    function onPostChildren($node,$request){}

    //-------------------------------------------------------------
    function onGetSelection() {

        if($this->exists("select")){
            $path=$this->get("select");
            if (substr($path, 0, 1)==  '/') {
                    $root=$this->root();
                    $path=substr($path, 1);

            }elseif ($this->selected !=null) {
                    $root=$this->selected;
            }else{
                    $root=$this;
            }

            return $root->search($path);
        }else{
            $result=new Store();
            if ($this->selected !=null) {
                $result->append($this->selected);
            }else{
                $result->append($this);
            }
            return $result;
        }

    }

    //-------------------------------------------------------------
    function test($node) {

        foreach($this->search("*/[Test]")->iter() as $test){
            #si test faux
            if(! $test->call($node)){return false;}
        }
        return true;
    }
    //-------------------------------------------------------------
    function call($request=[]) {

        $this->onPreCall($request);
        foreach($this->onGetSelection()->iter() as $node){

            if($this->test($node)){
                $this->onPreChildren($node,$request);
                foreach($this->search($this->get("select_children","*/[TreeSelect]"))->iter() as $action){
                    $action->selected=$node;
                    $action->call($request);
                }
                $this->onPostChildren($node,$request);
            }


        }
        $this->onPostCall($request);

    }
    //-------------------------------------------------------------

}
#=================================================================
?>
