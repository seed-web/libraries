<?php
 /**
  *
  * @package Interface
  *
  */
#=================================================================

class EventHandler extends TreeNode {

#=================================================================

    function do($node){
        foreach($node->search($this->get("select",""))->iter() as $elt){
            $data=new BaseClass($this);        
            $data->convert_from_node($elt);
            $this->onDo($elt,$data);
        }

    }
    function onDo($node,$data){}
}
 /**
  *
  * @package Interface
  *
  */
#=================================================================

class TreeWorkflow extends TreeNode{

#=================================================================

    function onPreSetup(){}
    function onPostSetup(){}

    function onPreCleanup(){}
    function onPostCleanup(){}

    //-------------------------------------------------------------
    function setup() {
        //echo "XXXX ".$this->path()."\n";
        $this->call_event("init");
        //show($this->globals);
        $this->convert_globals();
        $this->search("*/[EventHandler]")->execute("convert_globals");
        $this->call_event("setup");

        $this->onPreSetup();
        $this->search("*/[TreeWorkflow]")->execute("setup");
        //echo "     ".$this->path()."\n";
        $this->onPostSetup();
        //echo $this->path()."\n";
        //tree($this);
        //exit();
        //show( $this->search("/main/*"));
        $this->search("*/[EventHandler]/*/[TreeWorkflow]")->execute("setup");
        //echo "\n";
    }
    //-------------------------------------------------------------
    function cleanup() {

        $this->onPreCleanup();
        $this->search("*/[TreeWorkflow]")->execute("cleanup");
        $this->search("*/[EventHandler]/*/[TreeWorkflow]")->execute("cleanup");
        $this->onPostCleanup();
    }
    //-------------------------------------------------------------
    function call_event($name,$node=null) {
        //echo "event ".$name;
        if(!$node){$node=$this;}
        foreach($this->search("*/[EventHandler]/[event=".$name."]")->iter() as $action){
            //echo "event ".$name."  ".$action->path()."\n";
            $action->do($node);
        }
    }
    //-------------------------------------------------------------
}

 /**
  *
  * @package Interface
  *
  */
#=================================================================

class TreeRun extends TreeWorkflow{

#=================================================================

    function onPreCall($request=[]){}
    function onPostCall($request=[]){}
    //-------------------------------------------------------------
    function run($request=[]) {

        $this->setup();
        $this->call($request);
        $this->cleanup();
    }
    //-------------------------------------------------------------
    function call($request=[]) {

        $this->onPreCall($request);
        $this->children->by_class("TreeRun")->execute("call",$request);
        $this->onPostCall($request);
    }
    //-------------------------------------------------------------

}
#=================================================================

?>
