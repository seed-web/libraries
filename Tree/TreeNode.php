<?php
 /**
  *
  * @package Interface
  *
  */



$COUNT=0;


/**
* structure : arbre de données
*/
//=================================================================

class TreeNode extends BaseClass {

//=================================================================
    public $globals=null;
    public $parent=null;
    public $children;

    //-------------------------------------------------------------
    function __construct($parent=null,$data=[]) {

        $this->children=new Store();
        $this->parent=$parent;
        parent::__construct($data);
    }
    //-------------------------------------------------------------
    function onInit() {

        global $COUNT;
        $COUNT+=1;

        if ($this->exists("__class__") && ! $this->exists("name")){
            $this->set('name',$this->get("__class__")."_".$COUNT);

        }else if ( ! $this->exists("name")){
            $this->set('name',get_class($this)."_".$COUNT);
        }

        if ( $this->parent != null){
            $this->attach($this->parent);
        }

    }
    //-------------------------------------------------------------
    function onDestroy() {
        $this->clear();
        $this->detach();
    }
    //-------------------------------------------------------------
    //              TOOLS
    //-------------------------------------------------------------
    function is_instance($cls) {

        //if(get_class($this)== "TreeNode"){
        //    return $cls == $this->get("__class__");
        //}
        return $this instanceof $cls;
    }
    //-------------------------------------------------------------
    function __str__() {
        return get_class($this)." - ".$this->get("name");
        //return $this->get("__class__")." - ".$this->get("name");
    }
    //=============================================================
    //  GLOBALS
    //=============================================================
    //-------------------------------------------------------------
    function set_global($k,$v) {
        if($this->parent){
            $this->parent->set_global($k,$v);
        }else{
            if(!$this->globals){
                $this->globals=new TreeNode();
            }
            $real_dic=$this->globals->get_formated_dict();
            $this->globals->set(strtr($k, $real_dic),strtr($v, $real_dic));
        }
    }
    //-------------------------------------------------------------
    function get_globals() {
        if($this->parent){
            return $this->root()->get_globals();
        }else{
            if(!$this->globals){
                $this->globals=new TreeNode();
            }
            return $this->globals;
        }
    }
    //-------------------------------------------------------------
    function convert_globals($args=[]) {

        $data=new BaseClass($args);
        $data->update($this->get_globals());
        $real_dic=$data->get_formated_dict();
        //show($real_dic);

        foreach ($this->get_data() as $key=>$val ){
            $this->set( $key,strtr($val, $real_dic));
        }
        //show($this);
    }
    //-------------------------------------------------------------


    //=============================================================
    //  ATTACHEMENT
    //=============================================================

    //-------------------------------------------------------------
    function attach($parent) {

        $this->detach();

        $parent->children->append($this);
        $this->parent=$parent;
        $this->onAttach($parent);
    }
    //-------------------------------------------------------------
    function detach() {
        if ( $this->parent && $this->parent->children->exists($this)){
            $this->onDetach($this->parent);
            $this->parent->children->remove($this);
            $this->parent=null;
        }
    }
    //-------------------------------------------------------------
    function onAttach($parent) {}
    function onDetach($parent) {}

    //=============================================================
    //  PATH
    //=============================================================
    //-------------------------------------------------------------
    function name() {return $this->get("name");}
    function path() {return $this->get_path();}
    //-------------------------------------------------------------
    function get_path() {

        if($this->parent){
            return $this->parent->get_path().$this->get("name")."/";
        }else{
            return "/";
        }
    }
    //-------------------------------------------------------------
    function find($path) {
        if($path){
            $store=$this->search($path);
            if(! $store->is_empty()){
                return $store->nodes[0];
            }
        }
    }
    //-------------------------------------------------------------
    function search($select) {


        if (substr($select, 0, 1)== '/') {
                $root=$this->root();
        }else{
                $root=$this;
        }

        $nodes=new Store([$root]);

        foreach(explode("/",$select) as $elt){
            $nodes=$this->search_part($nodes,$elt);
            if($nodes->is_empty()){break;}
        }
        return $nodes;
    }
    //-------------------------------------------------------------
    function search_part($nodes,$select) {

        if ($select=="." || $select=="") {
            return $nodes;

        }else if($select==".."){
            $result=new Store();
            foreach($nodes->iter() as $node){
                if( $node->parent &&  ! $result->exists($node)){
                    $result->append($node->parent);
                }
            }
            return $result;

        }else if(substr($select, 0, 2) =='[!'){
            $cls=substr($select,2,-1);
//echo $cls;
            $result=new Store();
            foreach($nodes->iter() as $node){
                if( ! $node->is_instance($cls) &&  ! $result->exists($node)){
                    $result->append($node);
                }
            }
            return $result;

        }else if(substr($select, 0, 1) =='['){
            $select=substr($select,1,-1);

            if(strpos($select,"!=")>0){
                [$attr,$value] = explode("!=",$select);
                $result=new Store();
                foreach($nodes->iter() as $node){
                    if($node->exists($attr) and (string)$node->get($attr) != (string)$value &&  ! $result->exists($node)) {
                        $result->append($node);
                    }
                }
                return $result;

            }else if(strpos($select,"=")>0){

                [$attr,$value] = explode("=",$select);
                $result=new Store();
                foreach($nodes->iter() as $node){
                    if($node->exists($attr) and (string)$node->get($attr) == (string)$value &&  ! $result->exists($node)) {
                        $result->append($node);
                    }
                }
                return $result;

            }else{

                return $nodes->by_class($select);
            }

        }else{

            $result=new Store();

            foreach($nodes->iter() as $node){

                if($select=="*"){
                    $result->extend($node->children);
                }elseif($select=="**"){
                    $result->extend($node->descendants());
                }else{
                    //echo $select."\n";
                    $result->extend($node->children->by_attr_value("name",$select));
                    //show($result);
                }

            }
            //$result->remove_doublons();
            return $result;
        }

    }

    //=============================================================
    //  SEARCH
    //=============================================================
    function query($path,$action) {
        $node=$this->path->find($path);
        //echo "$path->$action()";
        return $node->$action();
    }
    //-------------------------------------------------------------
    //  SEARCH
    //-------------------------------------------------------------
    function clear() {

        foreach ($this->children->iter() as $node) {
            $node->destroy();
        }
    }
    //-------------------------------------------------------------
    //  SEARCH
    //-------------------------------------------------------------
    function root() {
        $root=$this;
        while ($root->parent !=null) {
            $root=$root->parent;
        }
        return $root;
    }
    //-------------------------------------------------------------
    function ancestors() {
        $result=new Store();
        $node=$this;
        while($node->parent){
            $node=$node->parent;
            $result->append($node);
        }
        return $result;

    }
    //-------------------------------------------------------------
    function descendants() {
        $result=new Store($this->children);
        $result = $result->transitive("children");
        //$result->remove($this);
        return $result;

    }

    //-------------------------------------------------------------
    function get_url() {
        return $this->root()->get_url().$this->path();
    }

    //-------------------------------------------------------------
    function make_node($path,$cls,$data) {
        $parent=$this->find($path);
        if ($parent){
            foreach($data as $key=>$value){
                //echo $key." - ".$data[$key]."\n";
                $parent->set($key,$value);
            }
        }else{
            $parent=$this;
            foreach(explode("/",$path) as $path_element){
                if($path_element !=""){
                    $node=$parent->find($path_element);
                    if ($node) {
                        $parent=$node;
                    }else{

                        try {
                            $parent= new $cls($parent,$data);
                            $parent->set("__class__",$cls);
                            $parent->set("name",$path_element);
                        }
                        catch (Error $e) {
                            $parent= new TreeNode($parent,$data);
                            $parent->set("__class__",$cls);
                            $parent->set("text",$e->getMessage());
                            $parent->data["name"]=$path_element;
                        }


                    }
                }
            }

        }
        return $parent;
    }

}
//=================================================================
?>
