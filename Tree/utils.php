<?php

function show($node){
    if($node instanceof Store) {
        foreach($node->iter() as $elt){
          echo "\n";
          show($elt);
        }
    }else if($node instanceof BaseClass) {
        foreach( $node->get_data() as $k=>$v){
            echo "    -".$k."   =   ".(string) $v."\n";
        }
    }else if($node instanceof Store) {
        foreach($node->iter() as $elt){
          echo "\n";
          show($elt);
        }    
    }else if(is_array($node)) {
        foreach( $node as $k=>$v){
            echo "    -".$k."   =   ".(string) $v."\n";
        }
    }else{
        foreach($node as $elt){
          echo "\n";
          show($elt);
        }       
    }
}
//-------------------------------------------------------------
function tree($node,$max=4,$level=1) {

if($level < $max){
    echo str_repeat("    | ",$level)."* ".$node->__str__()."\n";
        /*if($node->exists('text')) {
            echo str_repeat("    | ",$level)."  ".$node->get("text")."\n";
        } */
        //echo str_repeat("    | ",$level)."\n";
    foreach ($node->children->iter() as $child) {
        tree($child,$max,$level+1);
    }
}
}
//-------------------------------------------------------------


?>
