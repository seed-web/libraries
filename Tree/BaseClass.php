<?php
/**
*
* @package Interface
*
*
*/
//=================================================================

class BaseClass  {

//=================================================================

    public $__data;
    function onInit() {}
    function onDestroy() {}

    //-------------------------------------------------------------
    //              NEW/DESTROY
    //-------------------------------------------------------------
    function __construct($data=[]) {


        GLOBAL $COUNT;

        $this->__data=[];

        if( $data instanceof BaseClass ){
            $data=$data->get_data();
        }

        foreach($data as $key=>$value){
            $this->set($key,$value);
        }
    
        $this->onInit();

    }
    //-------------------------------------------------------------
    function destroy() {
        $this->onDestroy();
    }
    //-------------------------------------------------------------
    //              TOOLS
    //-------------------------------------------------------------
    function is_instance($cls) {
        return $this instanceof $cls;
    }
    //-------------------------------------------------------------
    function __str__() {
        return get_class($this);
    }
    //-------------------------------------------------------------
    //              DATA
    //-------------------------------------------------------------
    function get_data() {
        return $this->__data;
    }
    //-------------------------------------------------------------
    function get($attr,$default=null) {

        reset($this->__data);
        if ( array_key_exists($attr, $this->__data)) {
            return $this->__data[$attr];
        }else{
            return $default;
        }
    }
    //-------------------------------------------------------------
    function set($attr,$value) {
        $this->__data[$attr]=$value;
    }
    //-------------------------------------------------------------
    function exists($attr) {
        reset($this->__data);
        if(array_key_exists($attr,$this->__data)==1){
            return true;
        }else{
            return false;
        }
    }
    //-------------------------------------------------------------
    function keys() {
        return array_keys($this->__data);
    }
    //-------------------------------------------------------------
    function update($data) {
        if ($data instanceof BaseClass){
            foreach($data->__data as $key=>$value){
                $this->set($key,$value);
            }
        }else{
            foreach($data as $key=>$value){
                $this->set($key,$value);
            }
        }
    }
    //-------------------------------------------------------------
    //              GLOBALS
    //-------------------------------------------------------------
    function get_globals() {
        return [];
    }
    //-------------------------------------------------------------
    function get_formated_dict() {

        $real_dic=[];
        foreach ($this->__data as $key=>$val ){
            $real_dic[ "$(".$key.")" ]=$val;
        }
        return $real_dic;
    }
    //-------------------------------------------------------------
    function convert_from_node($node) {

        $real_dic=$node->get_formated_dict();
        //show($real_dic);

        foreach ($this->get_data() as $key=>$val ){
            $this->set( $key,strtr($val, $real_dic));
        }
        //show($this);
    }
    //-------------------------------------------------------------

}
//=================================================================
?>
