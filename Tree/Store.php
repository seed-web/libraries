<?php
 /**
  *
  * @package Interface
  *
  */



/*
    Store clone()
    Store reverse()
    same,in_self,in_list compare(list)
    Store difference(list)


    Store firsts(n=1)
    Store lasts(n=1)
    list all()
    Store by_class(cls)
    Store by_attr(attr)
    Store by_attr_value(attr,value)

    execute(function,args)
*/
#=================================================================

class Store {

#=================================================================
    public $nodes=[];

    //-------------------------------------------------------------
    function __construct($data=[]) {

          $this->extend($data);

    }
    //-------------------------------------------------------------
    function destroy() {

    }
    //-------------------------------------------------------------
    function size() {
      return count($this->nodes);
    }
    //-------------------------------------------------------------
    function is_empty() {
      return empty($this->nodes);
    }

    //-------------------------------------------------------------
    function iter() {
        return $this->copy();
    }
    //-------------------------------------------------------------
    function append($node) {
      $this->nodes[]=$node;
    }
    //-------------------------------------------------------------
    function remove($node) {
       if(($key = array_search($node, $this->nodes, false)) !== FALSE) {
            unset($this->nodes[$key]);
        }
        //$this->nodes=array_diff( $this->nodes, [$child]);
    }
    //-------------------------------------------------------------
    function exists($node) {
        return in_array($node,$this->nodes,true);
    }
    //-------------------------------------------------------------
    function extend($data) {

        if($data instanceof Store) {
            foreach($data->iter() as $child){
              $this->nodes[]=$child;
            }        
        }else{
            foreach($data as $child){
              $this->nodes[]=$child;
            }
        }
    }
    //-------------------------------------------------------------
    function clear() {
        unset( $this->nodes );
    }
    //-------------------------------------------------------------
    function copy() {
        $result=[];
        foreach( $this->nodes as $node ){
            $result[]=$node;
        }
        return $result;
    }
    //-------------------------------------------------------------
    function reverse() {

        $this->nodes=array_reverse($this->nodes);
        return $this;
    }

    //-------------------------------------------------------------
    function first() {

        if(! $this->is_empty()){
            return $this->nodes[0];
        }
    }
    //-------------------------------------------------------------
    function firsts($n=1) {
        $result=new Store();
        $result->extend( array_slice($this->nodes,0,$n) );
        return $result;
    }
    //-------------------------------------------------------------
    function lasts($n=1) {
        $result=new Store();
        $result->extend( array_slice($this->nodes,-$n) );
        return $result;
    }
    //-------------------------------------------------------------
    function by_class($cls) {
        $result=new Store();

        foreach($this->iter() as $node){

            if($node instanceof $cls) {
              $result->append( $node );
            }   
        }
        return $result;
    }
    //-------------------------------------------------------------
    function by_attr($attr) {
        $result=new Store();
        foreach($this->iter() as $node){
            if($node->exists($attr) ) {
              $result->append( $node );
            }   
        }
        return $result;
    }
    //-------------------------------------------------------------
    function by_attr_value($attr,$value) {
        $result=new Store();
        foreach($this->iter() as $node){
            if($node->exists($attr) and (string)$node->get($attr) == $value) {
              $result->append( $node );
            }   
        }
        return $result;
    }











    //-------------------------------------------------------------
    function remove_doublons() {
        $newlist=[];
        foreach($this->iter() as $elt){
            if(! in_array($elt, $newlist)){
                $newlist[]=$elt;
            }
        }
        $this->nodes=$newlist;
    }
    //-------------------------------------------------------------
    function transitive($attr) {
        $result=new Store();
        $result->extend($this);
        foreach($this->iter() as $child){
            $lst=$child->$attr->transitive($attr);
            $result->extend($lst);
        }
        return $result;
    }


    //-------------------------------------------------------------
    function plot() {
        echo $this->size()."\n";
        foreach($this->iter() as $elt){
            echo $elt->path()."\n";
        }
    }
    //-------------------------------------------------------------
    function execute2($action,$data=[]) {
        foreach($this->iter() as $elt){
            yield $elt->path() => $elt->$action(...$data);
        }
    }
    //-------------------------------------------------------------
    function execute($action,...$data) {
        foreach($this->iter() as $elt){
            $elt->$action(...$data);
        }
    }
    //-------------------------------------------------------------
}
#=================================================================
?>
